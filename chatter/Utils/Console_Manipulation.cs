﻿using System;
using System.Collections.Generic;
using System.Text;
using Chatter.Models;

namespace Chatter.Utils
{
    public static class Console_Manipulation
    {
        public static void SetConsoleDefaults()
        {
            Console.SetWindowSize(200, 60);
            Console.Title = "Chatter";
            Console.ForegroundColor = ConsoleColor.Red;
            // TODO: lock size
            Console.SetCursorPosition(0, (Console.CursorTop + 400));

            Sounds_Archive.Tetris();
        }

        public static void ClearCurrentConsoleLine()
        {
            Console.SetCursorPosition(1, Console.CursorTop - 1);
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }

        public static void PrintIncomingMessage(Message message, string myNickname)
        {
            switch (message.type)
            {
                case "join":
                    PrintWelcomeGoodbayMsg(message, true);
                    break;
                case "leave":
                    PrintWelcomeGoodbayMsg(message, false);
                    break;
                case "publish":
                    PrintPublishMsg(message, myNickname);
                    break;
                default:
                    break;
            }
        }

        private static void PrintPublishMsg(Message message, string myNickname)
        {
            bool isMine = myNickname == message.nickname;
            ConsoleColor color = isMine ? ConsoleColor.DarkRed : ConsoleColor.Cyan;
            Console.ForegroundColor = color;
            Console.WriteLine($":: {message.nickname } :: {message.message}");
            Console.ForegroundColor = ConsoleColor.Red;
        }

        private static void PrintWelcomeGoodbayMsg(Message message, bool join)
        {
            var Bcolor = Console.BackgroundColor;
            var Fcolor = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = join ? ConsoleColor.Green : ConsoleColor.DarkYellow;

            Console.WriteLine($":: {message.nickname } {GetRandomMessage(join ? Constants.WelcomeMsg: Constants.GoodbayMsg)}");

            Console.BackgroundColor = Bcolor;
            Console.ForegroundColor = Fcolor;
        }

        private static string GetRandomMessage(List<string> List)
        {
            Random bmf = new Random();
            return List[bmf.Next(List.Count)];
        }

        public static string GetBadMsgFormatMessage()
        {
            return GetRandomMessage(Constants.BadMsgFormat);
        }

        public static void PrintInvalidMessageFormat(string message)
        {
            var Bcolor = Console.BackgroundColor;
            var Fcolor = Console.ForegroundColor;
            Console.BackgroundColor = ConsoleColor.Red;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine($":: Invalid Incoming Message: {message}");
            Console.BackgroundColor = Bcolor;
            Console.ForegroundColor = Fcolor;
        }
    }
}
