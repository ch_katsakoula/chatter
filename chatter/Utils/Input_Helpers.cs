﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chatter.Utils
{
    public static class Input_Helpers
    {
        public static string getHostName()
        {
            Console.WriteLine("Type the rabbit HostName:");
            return Console.ReadLine();
        }

        public static string getNickname()
        {
            Console.WriteLine("Type your nickname:");
            return Console.ReadLine();
        }

        public static int getNowEpochTime()
        {
            return (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
        }

        public static DateTime convertEpochtoDateTime(long seconds)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).ToLocalTime().AddSeconds(seconds);
        }



    }
}
