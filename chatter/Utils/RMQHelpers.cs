﻿using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using Newtonsoft.Json;
using Chatter.Utils;
using Chatter.Intefaces;

using RabbitMQ.Client;
using Chatter.Models;

namespace Chatter.Utils
{
    public static class RMQHelpers
    {
        public static void queueDeclare(IModel channel, string nickname)
        {
            channel.QueueDeclare(
                queue: nickname,
                durable: true,
                exclusive: true,
                autoDelete: true,
                arguments: null);
        }

        public static void queueBind(IModel channel, string nickname)
        {
            channel.QueueBind(
                queue: nickname,
                  exchange: "chat_fnt",
                  routingKey: "");
        }

        public static byte[] EncodeMsg(Message message)
        {
            var serialized = JsonConvert.SerializeObject(message);
            var encoded = Encoding.UTF8.GetBytes(serialized);

            return encoded;
        }

    }
}
