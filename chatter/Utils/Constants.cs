﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chatter.Utils
{
    public static class Constants
    {
        public static List<string> BadMsgFormat = new List<string>()
        {
            "Who the f@ck sends inappropriate message format? Are you crazy??",
            "Behave! Change your message format.",
            "If you send one more crappy message of your I will kick you @ss"
        };

        public static List<string> WelcomeMsg = new List<string>()
        {
            "join the chat!!! Say hello"
        };

        public static List<string> GoodbayMsg = new List<string>()
        {
            "left the coneversation. Who cares?"
        };

    }
}
