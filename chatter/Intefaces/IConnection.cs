﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chatter.Intefaces
{
    public interface IRMQConnection
    {
        public void InitiateConnection();

        public void PublishMessage(string msg);
        public void PublishJoin();
        public void PublishLeave();
    }
}
