﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Chatter.Models
{
    public class Message
    {
        public string type { get; set; }
        public string nickname { get; set; }
        public int timestamp { get; set; }
        public string message { get; set; }

        public Message(string Type, string Nickname, int Timestamp)
        {
            type = Type;
            nickname = Nickname;
            timestamp = Timestamp;
        }
    }
}