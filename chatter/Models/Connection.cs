﻿using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using Newtonsoft.Json;
using Chatter.Utils;
using System.Collections.Generic;

using Chatter.Intefaces;


namespace Chatter.Models
{
    class Connection : IRMQConnection
    {
        private ConnectionFactory factory { get; set; }
        private EventingBasicConsumer consumer { get; set; }
        private IConnection connection { get; set; }
        private string nickname { get; set; }
        private IModel _channel_sub { get; set; }
        private IModel _channel_pub { get; set; }


        public void InitiateConnection()
        {
            List<string> HostNames = new List<string>();

            factory = new ConnectionFactory();

            HostNames.Add(Input_Helpers.getHostName());

            factory.HostName = HostNames[0];

            connection = factory.CreateConnection(HostNames);
            connection = factory.CreateConnection();
            nickname = Input_Helpers.getNickname();

            Console.WriteLine("You are connected. Wait for binding.");

            _channel_sub = connection.CreateModel();
            RMQHelpers.queueDeclare(_channel_sub, nickname);
            RMQHelpers.queueBind(_channel_sub, nickname);
            createConsumer();

            _channel_pub = connection.CreateModel();
            RMQHelpers.queueDeclare(_channel_pub, nickname);
            RMQHelpers.queueBind(_channel_pub, nickname);
        }

        // TODO implement WatchConnection
        public void WatchConnection() { }

        public void PublishMessage(string msg)
        {
            Message message = new Message(
                Type: "publish",
                Nickname: nickname,
                Timestamp: Input_Helpers.getNowEpochTime());
            message.message = msg;

            var encoded = RMQHelpers.EncodeMsg(message);

            Publish(encoded);
        }

        public void PublishLeave()
        {
            PublishAction(join: false);
        }

        public void PublishJoin()
        {
            PublishAction(join: true);
        }

        private void PublishAction(bool join)
        {
            Message message = new Message(
                Type: join ? "join" : "leave",
                Nickname: nickname,
                Timestamp: Input_Helpers.getNowEpochTime());

            var encoded = RMQHelpers.EncodeMsg(message);

            Publish(encoded);
        }

        private void Publish(byte[] encodedMsg)
        {
            _channel_pub.BasicPublish(
                exchange: "chat_fnt",
                routingKey: "",
                basicProperties: null,
                body: encodedMsg);
        }

        private void createConsumer()
        {
            consumer = new EventingBasicConsumer(_channel_sub);

            _channel_sub.BasicConsume(
                queue: nickname,
                    autoAck: true,
                    consumer: consumer);

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                var message = Encoding.UTF8.GetString(body);
                try
                {
                    Message deserialized = JsonConvert.DeserializeObject<Message>(message);
                    Console_Manipulation.PrintIncomingMessage(deserialized, nickname);
                }
                catch (Exception)
                {
                    Console_Manipulation.PrintInvalidMessageFormat(message);
                    PublishMessage(Console_Manipulation.GetBadMsgFormatMessage());
                }
            };
        }
    }
}
