﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Chatter.Models;
using Chatter.Intefaces;
using Chatter.Utils;
using App.Metrics.Formatters.Ascii;

namespace Chatter
{
    public class Worker : BackgroundService
    {
        private IRMQConnection _IRMQConnection;
        public Worker(IRMQConnection IRMQConnection)
        {
            _IRMQConnection = IRMQConnection;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Utils.AASCII_Archive.StartScreen();

            CancellationTokenSource inputCancelation = new CancellationTokenSource();

            Task Input = new Task(() =>
            {
                NavMenuService.MainMenu(inputCancelation, _IRMQConnection);
            }, inputCancelation.Token);

            Input.Start();

            return Task.CompletedTask;
        }
    }

}
