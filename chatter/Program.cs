﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Chatter.Intefaces;
using Microsoft.Extensions.Logging;

using Chatter.Models;
using Microsoft.Extensions.Hosting;
using Chatter.Utils;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace Chatter
{
    class Program
    {
        public static Task StartApp = new Task(() =>
        {
            Console_Manipulation.SetConsoleDefaults();
        });

        public static void Main(string[] args)
        {
            StartApp.Start();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                })
                .ConfigureServices(services =>
                {
                    services.AddSingleton<IRMQConnection, Connection>();
                    services.AddHostedService<Worker>();
                });



    }
}