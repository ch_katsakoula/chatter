﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Chatter.Models;
using Chatter.Intefaces;


namespace Chatter
{
    public static class NavMenuService
    {
        public static void StartChatting(CancellationTokenSource inputCancelation, IRMQConnection _IRMQConnection)
        {
            Console.WriteLine("You are ready. Start chatting!!");
            PrintChattingMenuDirections();

            _IRMQConnection.PublishJoin();
            while (!inputCancelation.IsCancellationRequested)
            {
                var input = Console.ReadLine();
                switch (input)
                {
                    case "help":
                        {
                            Console.WriteLine("To leave the conversation type: leave");
                            break;
                        }
                    case "leave":
                        {
                            _IRMQConnection.PublishLeave();
                            inputCancelation.Cancel();
                        }
                        break;
                    default:
                        _IRMQConnection.PublishMessage(input);
                        break;
                }
                if (!inputCancelation.IsCancellationRequested) Utils.Console_Manipulation.ClearCurrentConsoleLine();
            }
        }


        public static void MainMenu(CancellationTokenSource inputCancelation, IRMQConnection _IRMQConnection)
        {
            PrintMainMenuDirections();
            while (!inputCancelation.IsCancellationRequested)
            {
                var input = Console.ReadLine();
                switch (input)
                {
                    case "join":
                        {
                            _IRMQConnection.InitiateConnection();
                            Utils.Console_Manipulation.ClearCurrentConsoleLine();
                            StartChatting(inputCancelation, _IRMQConnection);
                            break;
                        }
                    case "help":
                        {
                            PrintMainMenuDirections();
                            break;
                        }
                    case "console-colors":
                        {
                            Console.WriteLine("Not implemented");
                            break;
                        }
                    case "wtf":
                        {
                            QuoteListMenu(inputCancelation, Utils.Constants.BadMsgFormat);
                            break;
                        }
                    case "welcome":
                        {
                            QuoteListMenu(inputCancelation, Utils.Constants.WelcomeMsg);
                            break;
                        }
                    case "goodbay":
                        {
                            QuoteListMenu(inputCancelation, Utils.Constants.GoodbayMsg);
                            break;
                        }
                }
            }
        }

        public static void QuoteListMenu(CancellationTokenSource inputCancelation, List<string> quoteList)
        {
            PrintQuoteMenuDirections();
            while (!inputCancelation.IsCancellationRequested)
            {
                var input = Console.ReadLine();
                if (input == "exit")
                {
                    break;
                }
                else if (input == "print")
                {
                    var index = 0;
                    quoteList.ForEach((i) =>
                    {
                        Console.WriteLine($"[{index}] {i}");
                        index++;
                    });
                }
                else if (input.StartsWith("-"))
                {
                    int index;
                    if (Int32.TryParse(input.Substring(1), out index) && index < quoteList.Count)
                    {
                        quoteList.RemoveAt(index);
                        Console.WriteLine("Quote deleted permanently");
                    }
                    else
                    {
                        Console.WriteLine($"Invalid input: [{input.Substring(1)}]. \nTo delete a quote type \"-\" and then the index number of the quote ( ex. -1 )");
                    }
                }
                else
                {
                    quoteList.Add(input);
                    Utils.Console_Manipulation.ClearCurrentConsoleLine();
                    Console.WriteLine($"[NEW QUOTE]: {input}");
                }
            }
        }
        public static void PrintQuoteMenuDirections()
        {
            Console.WriteLine("The Keyboard waits for your new quotes! After every new quote press [enter]");
            Console.WriteLine("To print the quote list in your screen type \"print\"");
            Console.WriteLine("To delete a quote type \"-\" and then the index number of the quote ( ex. -1 )");
            Console.WriteLine("To exit the WTH section type \"back\"");
        }

        public static void PrintMainMenuDirections()
        {
            Console.WriteLine("To add new Error messages type: wtf");
            Console.WriteLine("To join the conversation type: join");
            Console.WriteLine("To change the console colors type: console-colors");
        }

        public static void PrintChattingMenuDirections()
        {
            Console.WriteLine("To leave the conversation type: leave");
        }

    }
}
